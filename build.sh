#!/bin/sh

location=/home/mikolajr/Downloads
jre=serverjre-9-ea+181_linux-x64-musl_bin.tar.gz
jdk=jdk-9-ea+181_linux-x64-musl_bin.tar.gz
docker_user=mikolajr

if [ ! -d target ]; then
    mkdir -p target/jre target/jdk
    tar xzf ${location}/${jre} -C target/jre
    tar xzf ${location}/${jdk} -C target/jdk
fi

rm -f target/opt
ln -s jdk target/opt
docker build -t ${docker_user}/alpine-jdk9 .

rm -f target/opt
ln -s jre target/opt
docker build -t ${docker_user}/alpine-jre9 .
