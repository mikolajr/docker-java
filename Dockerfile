FROM alpine:3.6
MAINTAINER Mikolaj Rydzewski <mikolaj.rydzewski@gmail.com>

ENV JAVA_HOME /opt/jdk-9
ENV PATH ${PATH}:${JAVA_HOME}/bin

ADD target/opt /opt/
