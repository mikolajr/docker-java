# Java 9 docker image based on Alpine Linux

Since there is no official Java9 docker image yet I decided to create one myself.

In order to build JDK / JRE images one has to:

1. Download tar.gz files from http://jdk.java.net/9/ea
2. Update build.sh to use correct location for downloaded files
3. Update build.sh to use correct docker username

Build.

# Contributing

You are invited to contribute new features, fixes, or updates, large or small.
